#!/usr/bin/python
#-*- coding: utf-8-*-
'''
# Consultar dades que tenim a Postgres mitjançant una pipe amb PSQL.
# Select * from clientes.
# -----------------------------------------------------------------
# Escola del treball de Barcelona
# ASIX Hisi2 M06-ASO UF2NF1-Scripts
# ##########################################
'''
import sys
from subprocess import Popen, PIPE

#NOM_BASE = 'lab_clinic'
consulta = "select * from pacients;\q\n"
#command = "psql -qtA -F ';' %s" %(NOM_BASE)
command = "psql -qtA -F ';' clinic -U postgres -w jupiter -c 'select * from pacients;'"
com_remote = "psql -qtA -F ';' laboratori_clinic -h 192.168.2.46 -U postgres -w jupiter -c 'select * from pacients;'"
com_docker = "psql -h 172.17.0.2  -U edtasixm06 training -c 'select *  from oficinas;'"
cmd = "psql -h 172.17.0.2  -U edtasixm06 training"
sqlStatement = "select * from oficinas;\n\q\n"

# POPEN
pipeData = Popen(cmd, shell = True, stdin=PIPE, stdout=PIPE, stderr=PIPE)
pipeData.stdin.write(sqlStatement)
# Sortida
for line in pipeData.stdout.readlines():
	print line,
sys.exit(0)
