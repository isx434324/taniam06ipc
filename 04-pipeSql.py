#!/usr/bin/python
#-*- coding: utf-8-*-
'''
# Consultar dades que tenim a Postgres mitjançant una pipe amb PSQL.
# Select * from clientes.
# -----------------------------------------------------------------
# Escola del treball de Barcelona
# ASIX Hisi2 M06-ASO UF2NF1-Scripts
# ##########################################
'''
import sys
from subprocess import Popen, PIPE
import argparse

SQLSTATEMENT = 'select * from clientes where num_clie = 2111'
parser=argparse.ArgumentParser(description="consulta dades clients")
parser.add_argument("-c", dest="numclie", metavar='numCliente', default=SQLSTATEMENT, nargs='+' )
args = parser.parse_args()
print args
print args.numclie

cmd = "psql -h 172.17.0.2  -U postgres training"

# POPEN
pipeData = Popen(cmd, shell = True, stdin=PIPE, stdout=PIPE, stderr=PIPE)

for client in args.numclie :  
    sqlStatement = "select * from clientes where num_clie=%s;" % (client)
    print sqlStatement
    pipeData.stdin.write(sqlStatement+'\n')
    # Sortida
    for line in pipeData.stdout.readlines():
        print line,

pipeData.stdin.write('\q\n')


sys.exit(0)
