#!/usr/bin/python
#-*- coding: utf-8-*-
'''
# Consultar dades que tenim a Postgres mitjançant una pipe amb PSQL.
# Select * from clientes.
# -----------------------------------------------------------------
# Escola del treball de Barcelona
# ASIX Hisi2 M06-ASO UF2NF1-Scripts
# ##########################################
'''
import sys
from subprocess import Popen, PIPE
import argparse

SQLStatement = 'select * from oficinas'
parser=argparse.ArgumentParser(description="consulta SQL interactiu")
parser.add_argument(dest="sqlStatement", metavar='sentenciaSQL', default=SQLStatement )
args = parser.parse_args()

cmd = "psql -h 172.17.0.2  -U postgres training"

# POPEN
pipeData = Popen(cmd, shell = True, stdin=PIPE, stdout=PIPE, stderr=PIPE)
pipeData.stdin.write(args.sqlStatement+'\n\q\n')
# Sortida
for line in pipeData.stdout.readlines():
	print line,
sys.exit(0)
