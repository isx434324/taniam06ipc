#!/usr/bin/python
#-*- coding: utf-8-*-
# Exemples de argparse
## Tania 2018
### Extract de Python documentation
#Exemples interactius de argparser basics


import argparse
parser=argparse.ArgumentParser(description="super programa que fa la hostia de coses", prog="argparser", epilog="con esto i un bizcocho dema a les ocho")
parser.add_argument("ed", metavar='edat', type=int, nargs='+', help='edat (int)' )
parser.add_argument('fit', type=str, metavar="fitxer", nargs='+', help='a file (str)')
parser.add_argument('--verbosity', help="verbose help")
args = parser.parse_args()

print parser
print 20*"-"
print args
